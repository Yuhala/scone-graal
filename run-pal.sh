
#
# Copyright (c) 2020 Peterson Yuhala, IIUN
#
#!/bin/bash

#
# You must be in the graal-sgx directory as root here: ie make sure PWD = graal-sgx dir
# This script is used to run the corresponding program on the JVM in a SCONE container
#

APP_NAME="paldb"

APP_PKG="com.linkedin.paldb"
PKG_PATH="com/linkedin/paldb"

SVM_DIR="$PWD/substratevm"
SGX_DIR="$PWD/sgx"

APP_DIR="$SVM_DIR/$APP_NAME"
JAVAC="$JAVA_HOME/bin/javac"
#JAVA="$JAVA_HOME/bin/java"

LIB_BASE="$APP_DIR/lib/*"

GRAAL_HOME="$PWD/sdk/latest_graalvm_home/jre/lib/boot/graal-sdk.jar"

CP=$LIB_BASE:$GRAAL_HOME:$SVM_DIR:$APP_DIR

MAIN="BenchMain"

OTHERS=""
DB="$APP_DIR/data"

SCONE_OPTS="SCONE_VERSION=1 SCONE_LOG=7 SCONE_HEAP=2G"
#SCONE_OPTS=""



#clean old objects and rebuild svm if changed
OLD_OBJS=(/tmp/main.o main.so $APP_DIR/*.class)
cd $SVM_DIR
echo "---------------Removing old objects -----------"
for obj in $OLD_OBJS; do
    rm $obj
done

#mx clean
#rm -rf svmbuild
#mx build

#clean app classes
echo "--------------- Cleaning $APP_NAME classes -----------"
find $APP_DIR -name "*.class" -type f -delete

#compile app classes
BUILD_OPTS="-Xlint:unchecked -Xlint:deprecation"

echo "--------------- Compiling $APP_NAME application -----------"
#$JAVAC -cp $CP $BUILD_OPTS $APP_DIR/$PKG_PATH/$MAIN.java $OTHERS
javac -cp $CP $BUILD_OPTS $APP_DIR/$PKG_PATH/$MAIN.java $OTHERS

echo "--------------- Running $APP_PKG on JVM in SCONE container-----------"

#$SCONE_OPTS $JAVA_HOME/bin/java -cp $CP $APP_PKG.$MAIN 10000
$SCONE_OPTS java -cp $CP $APP_PKG.$MAIN 10000

