#!/bin/bash
#
# Script to build and run graphchi-java
# Author: Peterson Yuhala
#   
#

NUM_SHARDS=$1
PKG="edu.cmu.graphchi"
PKG_PATH="edu/cmu/graphchi"

# Main class
MAIN="BenchMain"

PIG="PigGraphChiBase"

BASE="$PWD"
LIBS="$PWD/lib/*"

CP="$LIBS:$BASE:."

BUILD_OPTS="-Xlint:unchecked -Xlint:deprecation "

DATA="$PWD/graphchi-data/fbedges"

function clean {
   find $BASE -name "*.class" -type f -delete    
}

# clean and recompile only for first run (with 10k keys)
if [[ $NUM_SHARDS -gt 1 ]]
then
    echo ""
else
    clean 
    # Build application
    javac -cp $CP $BUILD_OPTS $BASE/$PKG_PATH/apps/$MAIN.java $BASE/$PKG_PATH/hadoop/$PIG.java $BASE/$PKG_PATH/vertexdata/ForeachCallback.java
fi


# Run application

# Datasets: http://snap.stanford.edu/data/

# Page rank options:
# java -cp $CP edu.cmu.graphchi.apps.Pagerank [GRAPH-FILENAME] [NUM-OF-SHARDS] [FILETYPE]

java -cp $CP $PKG.apps.$MAIN $NUM_SHARDS