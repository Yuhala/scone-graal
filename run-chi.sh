#
# Copyright (c) 2020 Peterson Yuhala, IIUN
#
#!/bin/bash

#
# You must be in the graal-sgx directory as root here: ie make sure PWD = graal-sgx dir
# TODO: add script arguments to run these benchmarks automatically
#

APP_NAME="graphchi"

APP_PKG="edu.cmu.graphchi.apps"
PKG_PATH="edu/cmu/graphchi/apps"

SVM_DIR="$PWD/substratevm"
SGX_DIR="$PWD/sgx"

APP_DIR="$SVM_DIR/$APP_NAME"
JAVAC="$JAVA_HOME/bin/javac"
#JAVA="$JAVA_HOME/bin/java"

LIB_BASE="$APP_DIR/lib/*"

GRAAL_HOME="$PWD/sdk/latest_graalvm_home/jre/lib/boot/graal-sdk.jar"

CP=$LIB_BASE:$GRAAL_HOME:$SVM_DIR:$APP_DIR

MAIN="BenchMain"

#OTHERS="$APP_DIR/edu/cmu/graphchi/hadoop/PigGraphChiBase.java $APP_DIR/$PKG_PATH/vertexdata/ForeachCallback.java"
OTHERS=""
DATA="$APP_DIR/data/"

# /home/petman/projects/graal-tee/substratevm/graphchi/graphchi-data/fbedges 4 edgelist
# native image build options
SVM_OPTS=
#"-H:+UseOnlyWritableBootImageHeap -H:+ReportExceptionStackTraces"

#clean old objects and rebuild svm if changed
OLD_OBJS=("/tmp/main.o main.so" "$APP_DIR/*.class" "$SGX_DIR/Enclave/graalsgx/*.o" "$SGX_DIR/App/graalsgx/*.o")
cd $SVM_DIR

echo "--------------- Removing old objects --------------"
for obj in "${OLD_OBJS[@]}"; do
    rm $obj
done

function build_svm {
    mx clean
    rm -rf svmbuild
    mx build
}

#build_svm

#clean app classes
echo "--------------- Cleaning $APP_NAME classes -----------"
find $APP_DIR -name "*.class" -type f -delete

#compile app classes
BUILD_OPTS="-Xlint:unchecked -Xlint:deprecation -nowarn "

echo "--------------- Compiling $APP_NAME application -----------"
$JAVAC -cp $CP $BUILD_OPTS $APP_DIR/$PKG_PATH/$MAIN.java $OTHERS

#clean shards
rm -rf $DATA/gen.txt.* $DATA/*.bin

#run application in jvm to generate any useful configuration files: reflection, serialization, dynamic class loading etc
echo "--------------- Running $APP_PKG on JVM to generate useful config files-----------"
mkdir -p META-INF/native-image
$JAVA_HOME/bin/java -agentlib:native-image-agent=config-output-dir=META-INF/native-image -cp $CP $APP_PKG.$MAIN 4

#clean shards
#rm -rf $DATA/soc-LiveJournal1.txt.* $DATA/*.bin
rm -rf $DATA/gen.txt.* $DATA/*.bin

#exit 1

INITS="--initialize-at-run-time= \
 --initialize-at-build-time="

echo "--------------- Building $APP_NAME native image -----------"
NATIVE_IMG_OPTS="--shared --no-fallback --allow-incomplete-classpath "

#-H:+TraceClassInitialization
#--allow-incomplete-classpath
#--trace-class-initialization=org.springframework.util.ClassUtils

mx native-image -cp $CP $NATIVE_IMG_OPTS $APP_PKG.$MAIN

#copy new created object file to sgx module
cp /tmp/main.o $SGX_DIR/Enclave/graalsgx/
cp /tmp/main.o $SGX_DIR/App/graalsgx/
#copy generated headers to sgx module; graal entry points are defined here
mv $SVM_DIR/*.h $SGX_DIR/Include/
