#!/bin/bash
# Author: Peterson Yuhala
# Copyright 2021 IIUN

#HOME="/home/petman/projects/scone-graal/paldb"
NUM_KEYS=$1
HOME=$PWD
PKG_PATH="com/linkedin/paldb"
CP=$HOME
APP_DIR="."
BenchMain="$CP/$PKG_PATH/BenchMain.java"

MAIN="com.linkedin.paldb.BenchMain"

LIBS="$HOME/lib/*"
OPTS="-Xlint:deprecation -Xlint:unchecked"

function clean {
   find $APP_DIR -name "*.class" -type f -delete    
}

# clean and recompile only for first run (with 10k keys)
if [[ $NUM_KEYS -gt 10000 ]]
then
    echo ""
else
    clean 
    javac -cp $CP:$LIBS $BenchMain
fi

java -cp $CP:$LIBS $MAIN $NUM_KEYS


# Some useful commands that have no business with this script
# PYuhala

#apk add git
#apk update
#apk add bash
#apk add --no-cache \
#openssh-keygen
